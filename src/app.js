const addTaskModal = document.getElementById('add-modal');
const startAddTaskButton = document.querySelector('header button');
const backdrop = document.getElementById('backdrop');
const cancelAddTaskButton = addTaskModal.querySelector('.btn--passive');
const confirmAddTaskButton = cancelAddTaskButton.nextElementSibling;
const userInputs = addTaskModal.querySelectorAll('input');
const entryTextSection = document.getElementById('entry-text');
const deleteTaskModal = document.getElementById('delete-modal');

const tasks = [];

const toggleBackdrop = () => {
  backdrop.classList.toggle('visible');
};

const updateUI = () => {
  if (tasks.length === 0) {
    entryTextSection.style.display = 'block';
  } else {
    entryTextSection.style.display = 'none';
  }
};

const closeTaskDeletionModal = () => {
  toggleBackdrop();
  deleteTaskModal.classList.remove('visible');
};

const deleteTaskHandler = taskId => {
  let taskIndex = 0;
  for (const task of tasks) {
    if (task.id === taskId) {
      break;
    }
    taskIndex++;
  }
  tasks.splice(taskIndex, 1);
  const listRoot = document.getElementById('todo-list');
  listRoot.children[taskIndex].remove();
  closeTaskDeletionModal();
  updateUI();
};

const startDeleteTaskHandler = taskId => {
  deleteTaskModal.classList.add('visible');
  toggleBackdrop();

  const cancelDeletionButton = deleteTaskModal.querySelector('.btn--passive');
  let confirmDeletionButton = deleteTaskModal.querySelector('.btn--danger');

  confirmDeletionButton.replaceWith(confirmDeletionButton.cloneNode(true));

  confirmDeletionButton = deleteTaskModal.querySelector('.btn--danger');
    
  cancelDeletionButton.removeEventListener('click', closeTaskDeletionModal);

  cancelDeletionButton.addEventListener('click', closeTaskDeletionModal);
  confirmDeletionButton.addEventListener(
    'click',
    deleteTaskHandler.bind(null, taskId)
  );
};

const renderNewTaskElement = (id, title, time, rating) => {
  const newTaskElement = document.createElement('li');
  newTaskElement.className = 'todo-element';
  newTaskElement.innerHTML = `
    <div class="todo-element__info">
      <h2>${title}</h2>
      <h2>${time}</h2>
    </div>
  `;
  newTaskElement.addEventListener(
    'click',
    startDeleteTaskHandler.bind(null, id)
  );
  const listRoot = document.getElementById('todo-list');
  listRoot.append(newTaskElement);
};

const closeTaskModal = () => {
  addTaskModal.classList.remove('visible');
};

const showTaskModal = () => {
  addTaskModal.classList.add('visible');
  toggleBackdrop();
};

const clearTaskInput = () => {
  for (const usrInput of userInputs) {
    usrInput.value = '';
  }
};

const cancelAddTaskHandler = () => {
  closeTaskModal();
  toggleBackdrop();
  clearTaskInput();
};

const addTaskHandler = () => {
  const titleValue = userInputs[0].value;
  const timeValue = userInputs[1].value;

  const newTask = {
    id: Math.random().toString(),
    title: titleValue,
    time: timeValue
  };

  tasks.push(newTask);
  console.log(tasks);
  closeTaskModal();
  toggleBackdrop();
  clearTaskInput();
  renderNewTaskElement(
    newTask.id,
    newTask.title,
    newTask.time
  );
  updateUI();
};

const backdropClickHandler = () => {
  closeTaskModal();
  closeTaskDeletionModal();
  clearTaskInput();
};

startAddTaskButton.addEventListener('click', showTaskModal);
backdrop.addEventListener('click', backdropClickHandler);
cancelAddTaskButton.addEventListener('click', cancelAddTaskHandler);
confirmAddTaskButton.addEventListener('click', addTaskHandler);
